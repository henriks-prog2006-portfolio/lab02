module Reverse
    ( mreverse
    ) where

--
mreverse :: [a] -> [a]
mreverse [] = []
mreverse (x:xs) = mreverse xs ++ [x]
{-
calls mreverse on the tail of the list, and appends head to the reversed tail using ++
x is the head, xs is the tail
-}

-- | mreverse is my own implementation of list reversal
--
-- >>> mreverse "Hello"
-- "olleH"
--
-- >>> mreverse [1,2,3]
-- [3,2,1]
