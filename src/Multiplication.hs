module Multiplication
    ( mulTable
    ) where

-- Function to create a multiplication table for the input size
mulTable :: Int -> IO ()
mulTable size = do
  forLoop 1 size (\i -> do
    forLoop 1 size (\j -> do
      putStr (show (i * j) ++ "   "))
    putStrLn "")
{-
(\i -> do, lambda function that takes the argument i
->, separates argument from function
do, is used to perform io actions
show, converts the result of i*j into string
-}


-- Simple for loop function
forLoop :: Int -> Int -> (Int -> IO ()) -> IO ()
forLoop start end action = loop start
  where
    loop i
      | i <= end = do
          action i
          loop (i + 1)
      | otherwise = return ()
{-
takes 2 int values + a function that takes an int and returns io, and returns io
example: takes 1 and size as ints, lambda as function and returns io
forloop takes the argument "start" and "end", and the function "action" to be executed on each iteration
= loop start, initializes the inner function "loop" with "start" as its value
loop, takes the argument "i"
calls the action function with the current value of i, then recursively calls itself with i+1
-}

-- | Generates a multiplication table for a given size and prints it to the console.
--
-- >>> mulTable 5
--   1   2   3   4   5
--   2   4   6   8   10
--   3   6   9   12   15
--   4   8   12   16   20
--   5   10   15   20   25
