module Oldest
    ( parseLine, findOldestStudents
    ) where

-- | Function to parse a line into a tuple of (first name, last name, age)
--
-- >>> parseLine "John Doe 25"
-- ("John","Doe",25)
--
-- >>> parseLine "Alice Smith 30"
-- ("Alice","Smith",30)
--
-- >>> parseLine "Bob Johnson 22"
-- ("Bob","Johnson",22)
parseLine :: String -> (String, String, Int)
parseLine line =
    let [first, last, ageStr] = words line
    in (first, last, read ageStr :: Int)
{-
words splits a string by separating after spaces
read turns a string into int
-}
-- | Function to find the oldest students
--
-- >>> findOldestStudents [("John", "Doe", 25), ("Alice", "Smith", 30), ("Bob", "Johnson", 22)]
-- [("Alice","Smith")]
--
-- >>> findOldestStudents [("John", "Doe", 25), ("Alice", "Smith", 30), ("Bob", "Johnson", 30)]
-- [("Alice","Smith"),("Bob","Johnson")]
findOldestStudents :: [(String, String, Int)] -> [(String, String)]
findOldestStudents students =
    let oldestAge = maximum $ map (\(_, _, age) -> age) students
        oldestStudents = filter (\(_, _, age) -> age == oldestAge) students
    in map (\(first, last, _) -> (first, last)) oldestStudents
{-
$ is for avoiding parentheses, saying that everything to the right should be the argument for the
function to the left
->, separates parameters from the body, so the lambda takes a list and returns only age
map applies the lambda function to students to extract a list of ages
filter applies the lambda function to students and only keeps those that are True
map then creates a new list without the age for the oldest students
-}

