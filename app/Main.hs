module Main (main) where

import Lib
import Reverse
import Multiplication
import Oldest


main :: IO ()
main = do
  putStrLn "Hello backwards is:"
  print (mreverse "Hello")

  putStrLn "Enter your desired size of the multiplication table:"
  size <- readLn
  mulTable size

  -- Read input from stdin
  putStrLn "List of students (end with Ctrl + Z then Enter):"
  input <- getContents
  -- Parse input lines into a list of tuples
  let students = map parseLine (lines input)
  -- Find the oldest students
  let oldest = findOldestStudents students
  -- Print the amount of oldest students
  putStrLn $ "Count: " ++ show (length oldest)
  {- lines splits the input into a list of string for parseLine function
  map, applies the function parseline to each element of the list from "lines"
  let then binds students to the result
  --
  oldest appliest the findOldestStudents function to the list
  show turns "length" into a string
  -}