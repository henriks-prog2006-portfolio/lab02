# Lab2 - Lists and Recursion (Haskell)



## Learning objectives for Lab2, as outlined by lecturer

> - Learning the basics of haskell types
> - Learning the basics of list processing in Haskel



----------------


# The tasks

### Task 1: Reverse a list
```
Write your own function, that given a list, returns the same list in reverse order.
There is a built-in function called reverse but you cannot use it for your own
implementation.
```
#### Input:
```
Hello
```
#### Output:
```
"olleH"
```
--------------
### Task 2: Multiplication table
```
Write a program that prints a matrix for Multiplication for a given size, nicely formatted on the screen.
The function pads everything to occupy 3 ascii slots.
```
#### Input:
```
5
```
#### Output:
```
1   2   3   4   5
2   4   6   8   10
3   6   9   12   15
4   8   12   16   20
5   10   15   20   25
```
--------------
### Task 3: Oldest students count
```
Given an input file in specific format, find which students are the oldest, and count them.
Use getContents, lines and words to parse the input file.
The use of parsec library to parse input files is forbidden.
```
#### Input:
```
Alice Cooper 25
Alice Boa 23
Bob Marley 23
Alice Chains 25
Charlie Brown 21
Charlie Chaplin 25
Eve Wonder 24
Sandra White 21
```
#### Output:
```
Count: 3
```

